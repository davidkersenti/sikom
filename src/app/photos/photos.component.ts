import { Component, OnInit } from '@angular/core';
import {PhotosService} from './photos.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styles: [`
    .photos li { cursor: default; }
    .photos li:hover { background: #ecf0f1; } 
  `]
})
export class PhotosComponent implements OnInit {
  photos;

  currentPhoto;

  isLoading = true;

  constructor(private _photosService: PhotosService) {
    //this.users = this._userService.getUsers();
  }
   deletePhoto(photo){

    this.photos.splice(
      this.photos.indexOf(photo),1
    )
  }

    addPhoto(photo){
    this.photos.push(photo)
  }
  editPhoto(originalAndEdited){
    this.photos.splice(

      this.photos.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.photos);
  } 
  

 
 ngOnInit() {
        this._photosService.getPhotos()
			    .subscribe(photos => {this.photos = photos;
                               this.isLoading = false});
  }

}
