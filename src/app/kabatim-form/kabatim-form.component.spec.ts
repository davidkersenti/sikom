/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { KabatimFormComponent } from './kabatim-form.component';

describe('KabatimFormComponent', () => {
  let component: KabatimFormComponent;
  let fixture: ComponentFixture<KabatimFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KabatimFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KabatimFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
