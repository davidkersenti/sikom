import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Kabatim} from '../kabatim/kabatim'

@Component({
  selector: 'app-kabatim-form',
  templateUrl: './kabatim-form.component.html',
  styleUrls: ['./kabatim-form.component.css']
})
export class KabatimFormComponent implements OnInit {

   @Output() kabatimAddedEvent = new EventEmitter<Kabatim>();
  kabatim:Kabatim = {
    name: '',
    age: ''
  };

  constructor() { }
  onSubmit(form:NgForm){
    console.log(form);
    this.kabatimAddedEvent.emit(this.kabatim);
    this.kabatim = {
       name: '',
       age: ''
    }
  }


  ngOnInit() {
  }

}
