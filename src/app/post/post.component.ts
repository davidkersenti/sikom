import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
   inputs:['post']
})
export class PostComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post[]>();
  post:Post;
  isEdit : boolean = false;
  editButtonText = 'Edit';
  tempPost:Post = {title:null,auther:null};

  constructor() { }

   sendDelete(){
    this.deleteEvent.emit(this.post);
  }
   cancelEdit(){
    this.isEdit = false;
    this.post.auther = this.tempPost.auther;
    this.post.title = this.tempPost.title;

    this.editButtonText = 'Edit'; 
  }
    toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     if(this.isEdit){
       this.tempPost.auther = this.post.auther;
       this.tempPost.title = this.post.title;
     } else {
       let originalAndNew = [];
       originalAndNew.push(this.tempPost,this.post);
       this.editEvent.emit(originalAndNew);
     }
  }

  ngOnInit() {
  }

}
