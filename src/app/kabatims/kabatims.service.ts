import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';

import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class KabatimsService {
   private _url = "http://davidke.myweb.jce.ac.il/api/kabatims/";

 constructor(private _http:Http ) { }

 getKabatimFromRestApi(){
    let url = this._url;
    return this._http.get(url).map(res => res.json()).delay(2000)
  }

}
