import { Component, OnInit } from '@angular/core';
import {KabatimsService} from './kabatims.service';

@Component({
  selector: 'app-kabatims',
  templateUrl: './kabatims.component.html',
 styles: [`
    .kabatims li { cursor: default; }
    .kabatims li:hover { background: #ecf0f1; } 
  `]
})
export class KabatimsComponent implements OnInit {

  kabatims;



 currentKabatims;

 isLoading = true;

   constructor(private _kabatimsService: KabatimsService) {
    //this.users = this._userService.getUsers();
  }
   
  deleteKabatim(kabatim){
    this.kabatims.splice(
      this.kabatims.indexOf(kabatim),1
    )
  }
   addKabatim(kabatim){
    this.kabatims.push(kabatim)

  }


  editKabatim(originalAndEdited){
    this.kabatims.splice(
      this.kabatims.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.kabatims);
  } 
  

  
  
   
   

  
  ngOnInit() {
        this._kabatimsService.getKabatimFromRestApi()
			    .subscribe(kabatims => {this.kabatims = kabatims.result;
                               this.isLoading = false;
                               console.log(this.kabatims)});
  }

}
