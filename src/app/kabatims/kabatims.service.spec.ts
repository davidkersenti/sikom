/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { KabatimsService } from './kabatims.service';

describe('KabatimsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KabatimsService]
    });
  });

  it('should ...', inject([KabatimsService], (service: KabatimsService) => {
    expect(service).toBeTruthy();
  }));
});
