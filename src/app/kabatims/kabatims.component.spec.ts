/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { KabatimsComponent } from './kabatims.component';

describe('KabatimsComponent', () => {
  let component: KabatimsComponent;
  let fixture: ComponentFixture<KabatimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KabatimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KabatimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
