/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { KabatimComponent } from './kabatim.component';

describe('KabatimComponent', () => {
  let component: KabatimComponent;
  let fixture: ComponentFixture<KabatimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KabatimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KabatimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
