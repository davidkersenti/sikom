import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Kabatim} from './kabatim'

@Component({
  selector: 'app-kabatim',
  templateUrl: './kabatim.component.html',
  styleUrls: ['./kabatim.component.css'],
  inputs:['kabatim']
})
export class KabatimComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Kabatim>();
  @Output() editEvent = new EventEmitter<Kabatim[]>();

  
  kabatim:Kabatim;
   tempKabatim:Kabatim = {age:null,name:null};

    isEdit : boolean = false;
  editButtonText = 'Edit';
   

  constructor() { }
   sendDelete(){

    this.deleteEvent.emit(this.kabatim);
  }

    cancelEdit(){
    this.isEdit = false;
    this.kabatim.age = this.tempKabatim.age;
    this.kabatim.name = this.tempKabatim.name;
    this.editButtonText = 'Edit'; 
  }

 toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     if(this.isEdit){
       this.tempKabatim.age = this.kabatim.age;
       this.tempKabatim.name = this.kabatim.name;
     } else {
       let originalAndNew = [];
       originalAndNew.push(this.tempKabatim,this.kabatim);
       this.editEvent.emit(originalAndNew);
     }
  }


  
  



  ngOnInit() {
  }

}
