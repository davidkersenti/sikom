import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; } 
  `]
})
export class PostsComponent implements OnInit {

  posts;
   currentPost;

 
 isLoading = true;

 

  addPost(post){
    this.posts.push(post)
  }

  deletePost(post){
    this.posts.splice(
      this.posts.indexOf(post),1
    )
  }
  editUser(originalAndEdited){
    this.posts.splice(

      this.posts.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.posts);
  }  

   constructor(private _postsService: PostsService) {
    this.posts = this._postsService.getPosts();
  }

  ngOnInit() {
  }

}
