import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
 import { UsersService } from './users/users.service';
import { UserFormComponent } from './user-form/user-form.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { PostFormComponent } from './post-form/post-form.component';
import { PhotosComponent } from './photos/photos.component';
import { PhotosService } from './photos/photos.service';
import { PhotoComponent } from './photo/photo.component';
import { PhotoFormComponent } from './photo-form/photo-form.component';
import { KabatimsComponent } from './kabatims/kabatims.component';
import { KabatimComponent } from './kabatim/kabatim.component';
import { KabatimsService} from './kabatims/kabatims.service';
import { KabatimFormComponent } from './kabatim-form/kabatim-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import {  InvoicesService} from './invoices/invoices.service';
import { InvoiceComponent } from './invoice/invoice.component';
import{AngularFireModule} from 'angularfire2';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';

export const firebaseConfig = {
   apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
}





const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'photos', component: PhotosComponent },
  { path: 'kabatims', component: KabatimsComponent },
  { path: 'kabatimForm', component: KabatimFormComponent },
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent },
  
  { path: 'photoForm', component: PhotoFormComponent },  
  { path: '', component: InvoicesComponent },
  { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostsComponent,
    PostComponent,
    PostFormComponent,
    PhotosComponent,
    PhotoComponent,
    PhotoFormComponent,
    KabatimsComponent,
    KabatimComponent,
    KabatimFormComponent,
    InvoicesComponent,
    InvoiceComponent,
    InvoiceFormComponent,
    SpinnerComponent,
   
   
   
  
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
     AngularFireModule.initializeApp(firebaseConfig)
   
  ],
  providers: [UsersService,PostsService,PhotosService,KabatimsService,InvoicesService],
  bootstrap: [AppComponent],
})
export class AppModule { }
