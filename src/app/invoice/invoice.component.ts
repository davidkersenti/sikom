import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from './invoice'

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
   @Output() deleteEvent = new EventEmitter<Invoice>();
   @Output() editEvent = new EventEmitter<Invoice>();
invoice:Invoice;
 isEdit : boolean = false;
  editButtonText = 'Edit';
  tempInvoice:Invoice = {amount:null,name:null};
  
  
  constructor() { }

  sendDelete(){

    this.deleteEvent.emit(this.invoice);
  }
    toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
    
     if(this.isEdit){
       this.tempInvoice.amount = this.invoice.amount;
       this.tempInvoice.name = this.invoice.name;
     } else {     
       this.editEvent.emit(this.invoice);
     }
  }

  ngOnInit() {
  }

}
