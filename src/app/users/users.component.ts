import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
 styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
  `]
})
export class UsersComponent implements OnInit {

    users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]

  addUser(user){
    this.users.push(user)

  }
   deleteUser(user){
    this.users.splice(
      this.users.indexOf(user),1
    )
  }
  
  editUser(originalAndEdited){
    this.users.splice(
      this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.users);

  }

  constructor() { }

  ngOnInit() {
  }

}
