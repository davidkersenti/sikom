import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Photo} from '../photo/photo'

@Component({
  selector: 'app-photo-form',
  templateUrl: './photo-form.component.html',
  styleUrls: ['./photo-form.component.css']
})
export class PhotoFormComponent implements OnInit {

  @Output() photoAddedEvent = new EventEmitter<Photo>();

  photo:Photo = {
    id: '',
    title: '',
    url: '',
  };

  constructor() { }

  
  onSubmit(form:NgForm){
    console.log(form);
    this.photoAddedEvent.emit(this.photo);
    this.photo = {
       id: '',
       title: '',
       url: '',

    }

  }

  ngOnInit() {
  }

}
