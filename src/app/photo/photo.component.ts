import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Photo} from './photo'

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css'],
   inputs:['photo']
})
export class PhotoComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Photo>();
   @Output() editEvent = new EventEmitter<Photo[]>();
  photo:Photo;
  tempPhoto:Photo = {id:null,title:null,url:null};

    isEdit : boolean = false;
  editButtonText = 'Edit';

  constructor() { }

   sendDelete(){
    this.deleteEvent.emit(this.photo);
  }
  
  cancelEdit(){
    this.isEdit = false;
    this.photo.title = this.tempPhoto.title;
    this.photo.id = this.tempPhoto.id;
    this.photo.url = this.tempPhoto.url;
    this.editButtonText = 'Edit'; 
  }

   toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     if(this.isEdit){
       this.tempPhoto.title = this.photo.title;
       this.tempPhoto.id = this.photo.id;
       this.tempPhoto.url = this.photo.url;
     } else {
       let originalAndNew = [];
       originalAndNew.push(this.tempPhoto,this.photo);
       this.editEvent.emit(originalAndNew);
     }
  }

  ngOnInit() {
  }

}
